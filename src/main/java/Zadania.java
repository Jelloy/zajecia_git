import java.util.Arrays;

public class Zadania {
    public static void findHighestAndLowest(int[] tab){
        int min = tab[0];
        int max = tab[0];

        for(int i=0; i<tab.length; i++){
            if(min > tab[i]){
                min = tab[i];
            }
            if(max < tab[i]){
                max = tab[i];
            }
        }
        System.out.println("Najmniejsza wartość w tablicy to: "+min);
        System.out.println("Największa wartość w tablicy to: "+max);
    }
    public static void reverseArray(int[] tab){
        System.out.println(Arrays.toString(tab));

        for(int i=0; i<tab.length/2; i++){
            int temp = tab[i];
            tab[i] = tab[tab.length -i - 1];
            tab[tab.length -i -1] = temp;
        }
        System.out.println(Arrays.toString(tab));
    }

    public static int mostFrequent (int[] tab){
        int count = 0;
        int maxCount = 0;
        int id = 0;
        for(int i=0; i<tab.length; i++){
            for(int j=0; j<tab.length; j++){
                if(tab[i] == tab[j]){
                    count++;
                }
                if(maxCount<count){
                    maxCount = count;
                    id = i;
                }
                count=0;
            }
        }
        return tab[id];
    }

    public static int[] mergeArrays(int[] tabA, int[] tabB){
        int tableLenght = tabA.length+tabB.length;
        int[] tabC = new int [tableLenght];

        for(int i=0; i<tabC.length; i++){
            if(i<tabA.length){
                tabC[i]=tabA[i];
            }
            if(i>=tabA.length){
                tabC[i]=tabB[i-tabA.length];
            }
        }
        return tabC;
    }

    public static void Fibonacci(int max) {
        int a1 = 1;
        int a2 = 1;
        for (int i = 0; i < max; i++) {
            if (i < 2) {
                System.out.print(1 + " ");
            } else {
                int sum = a2 + a1;
                System.out.print(sum + " ");
                a2 = a1;
                a1 = sum;
            }
        }
    }

    public static int[] convertToString(int number){
       String numberString = String.valueOf(number);
       int[] numTab = new int[numberString.length()];

       for(int i=0; i<numberString.length(); i++){
           String charStr = numberString.substring(i, i+1);
           numTab[i] = Integer.valueOf(charStr);
       }
       return numTab;
    }

    public static int reverseInteger(int number){
        int reversed = 0;
        while(number != 0) {
            int digit = number % 10;
            reversed = reversed * 10 + digit;
            number /= 10;
        }
        return reversed;
    }

    public static int[] addTable(int[] arr1, int[] arr2){
        if(arr1==null){
            return arr1;
        }
        if(arr2==null){
            return arr2;
        }
        int[] result = new int[arr1.length > arr2.length ? arr1.length : arr2.length];

        int arr1Index = arr1.length -1;
        int arr2Index = arr2.length -1;

        //zmienna do zapisywania w pamięci, kiedy suma na pozycji nie mieści się w przedziale 0-9
        int mem = 0;

        //wykonujemy operacje od prawej strony tablicy
        for (int i = result.length-1; i>=0; i--){
            int tmp = 0;
            //jeśli oba indeksy są poprawne sumujemy liczby na danych pozycjach
            if(arr1Index >= 0 && arr2Index >= 0){
                tmp = arr1[arr1Index]+arr2[arr2Index];
            }
            //indeks arr1 jest poza zakresem, więc tylko przepisujemy z tablicy arr2
            else if(arr1Index<0){
                tmp = arr2[arr2Index];
            }
            //indeks arr2 jest poza zakresem
            else{
                tmp = arr1[arr1Index];
            }
            //jeśli mamy w pamięci coś z poprzedniej operacji to dodajemy i czyścimy pamięć
            if(mem > 0){
                tmp+=mem;
                mem=0;
            }
            if(tmp > 9){
                mem = 1;
                tmp = tmp % 10;
            }
            //wstawiamy wynik do tablicy wynikowej
            result[i] = tmp;

            //przesuwamy indeksy o jeden w lewo
            arr1Index--;
            arr2Index--;
        }
        //po zakończeniu jeżeli ciągle mamy coś w pamięci
        if(mem > 0 ){
            //alokujemy nową tablicę żeby zmieścić nową pozycję
            int[] tmpArr = new int[result.length+1];
            //na początek nowej tablicy wstawiamy wartość z pamięci
            tmpArr[0] = mem;
            for(int i =0; i<result.length; i++){
                tmpArr[i+1] = result[i];
            }
            result = tmpArr;

        }
        return result;
    }

    public static void showASCII(char a){
        int ascii = (int) a;
        System.out.println("Znak "+a+" posiada kod "+ascii);
    }
    public static void ASCIItoChar(int ascii){
        char a = (char) ascii;
        System.out.println("Pod kodem "+ascii+" jest znak "+a);
    }


}
