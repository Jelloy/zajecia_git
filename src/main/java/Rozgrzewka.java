import java.util.Scanner;

public class Rozgrzewka {
    public static void from01to100() {
        for (int i = 1; i <= 100; i++) {
            System.out.print(i + " ");
        }
    }

    public static void fromXtoY(){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        if(a<b){
            for(int i=a; i<=b; i++){
                System.out.println(i+" ");
            }
        }
        else if(b<a){
            for(int i=b; i<=a; i++){
                System.out.println(i+" ");
            }
        }
        else if(a==b){
            System.out.println("Podałeś dwie takie same liczby");
        }
    }
    public static void fromXtoYwithZ(){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int z = scanner.nextInt();

        if(a<b){
            for(int i=a; i<=b; i+=z){
                System.out.println(i+" ");
            }
        }
        else if(b<a){
            for(int i=b; i<=a; i+=z){
                System.out.println(i+" ");
            }
        }
        else if(a==b){
            System.out.println("Podałeś dwie takie same liczby");
        }
    }

    public static void isEvenNumber(){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if(a%2==0){
            System.out.println(a+" jest parzysta");
        }else{
            System.out.println(a+" nie jest parzysta");
        }
    }
}
