public class CesarCrypto {

    private static final int NUM_CHARS = 'z' - 'a';
    private int offset;

    public CesarCrypto(int offset) {
        this.offset = offset & NUM_CHARS;
    }

    //szyfrowanie wiadomości

    public String cipher(String input){
        // zamiana liter na małe
        String data = input.toLowerCase();
        StringBuilder builder = new StringBuilder();
        //pętla która przechodzi po każdym znaku wiadomości którą szyfrujemy
        for(int i = 0; i < data.length(); i++){
            char character = data.charAt(i);
            if(character >='a' && character <= 'z'){
                //przesunięcie znaku
                character+=offset;

                //sprawdzenie czy nie nastąpiło przekroczenie zakresu (a-z)
                if(character>'z'){
                    //zawinięcie znaku
                    character =(char) ((character - 'z') + 'a');
                }
            }
            builder.append(character);
        }
        return builder.toString();
    }
}
